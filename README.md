# edent.tel
The site which powers https://edent.tel/

A semantic "about" page.

Blog post: https://shkspr.mobi/blog/2017/04/how-i-built-a-responsive-semantic-contact-me-page-in-under-16kb/

## Licenses

* [MarvelApp CSS](https://github.com/marvelapp/devices.css/) - MIT

### Images

All icons can be found at https://github.com/edent/SuperTinySocialIcons/
