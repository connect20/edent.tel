<?php

$html = '<!doctype html>
<html lang="en-GB">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Terence Eden\'s Contact Details</title>
	<meta name="description" content="@edent all over the web">
	<meta name="author" content="Terence Eden">

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:creator" content="@edent" />
	<meta property="og:url" content="https://edent.tel/" />
	<meta property="og:title" content="Contact @edent" />
	<meta property="og:description" content="Terence Eden\'s contact details - voice, text, fax. OK. Maybe not fax…" />
	<meta property="og:image" content="https://edent.tel/preview.png" />
	<meta property="og:image:width"  content="512" />
	<meta property="og:image:height" content="512" />

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<meta name="theme-color" content="#fff">
	<meta name="flattr:id" content="49zjz5">
	<style>';
		$css = file_get_contents("css/edent.css");
		$css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
		$css = str_replace(': ' , ':', $css);
		$css = str_replace(' {' , '{', $css);
		$css = str_replace(' ,' , ',', $css);
		$css = str_replace(' !' , '!', $css);
		$css = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css);

		$html .= $css;
	$html .='</style>
	<base target="_blank">
</head>
<body>
<div class="marvel-device htc-one">
	<div class="top-bar"></div>
	<div class="camera"></div>
	<div class="sensor"></div>
	<div class="speaker"></div>
	<div class="screen">
		<main itemscope itemtype="https://schema.org/Person" class="h-card" rel="me">
			<header itemprop="name" class="p-name">
				<h1>
					<span itemprop="honorificPrefix" class="none">
						<span itemprop="gender" itemscope itemtype="http://schema.org/Male">Mr</span>
					</span>&nbsp;
					<span itemprop="givenName">Terence</span>&nbsp;
					<span itemprop="familyName">Eden</span>&nbsp;
				</h1>
				<small>
					<span itemprop="honorificSuffix">BSc &nbsp;
						<span itemprop="alumniOf" itemscope itemtype="https://schema.org/CollegeOrUniversity">(
							<abbr itemprop="name" title="University of East Anglia">UEA</abbr>
							<link itemprop="sameAs" href="https://uea.ac.uk">
						</span>)
					</span>,&nbsp;
					<abbr itemprop="honorificSuffix" title="Member of the British Computer Society">MBCS</abbr>,&nbsp;
					<abbr itemprop="honorificSuffix" title="Member of the Institution of Engineering and Technology">MIET</abbr>
				</small>
			</header>
			<p itemprop="address" itemscope itemtype="https://schema.org/PostalAddress" class="none">
				<span itemprop="addressLocality">London</span>, <span itemprop="addressCountry">UK</span>
			</p>
			<h2 class="p-note">
				<span itemprop="jobTitle">Head of Open Technology</span> at <span itemprop="worksFor" itemscope itemtype="http://schema.org/Organization"><span itemprop="name">NHSX</span><link itemprop="sameAs" href="https://nhsx.nhs.uk"></span>
			</h2>
			<p title="I speak a little Chinese" lang="zh">我说一点中文。</p>
			<p><a href="edent.vcf" class="download">Download my contact details</a></p>
			<link itemprop="image" href="https://edent.tel/avatar">
			<span class="none">
				<span itemprop="memberOf" itemscope itemtype="http://schema.org/Organization">
					<span itemprop="name">BCS, The Chartered Institute for IT</span>
					<link itemprop="sameAs" href="https://www.bcs.org/">
				</span>
				<span itemprop="memberOf" itemscope itemtype="http://schema.org/Organization">
					<span itemprop="name">Institution of Engineering and Technology</span>
					<link itemprop="sameAs" href="https://www.theiet.org">
				</span>
			</span>
			<nav>';

	$str = file_get_contents('config.json');
	$json = json_decode($str, true);

	foreach ($json as $key => $img) {
		$html .= '<div class="icon">';
			$link     = ($img["link"]     != null) ? "href=\"{$img["link"]}\"" : "";
			$itemprop = ($img["itemprop"] != null) ? "itemprop=\"{$img["itemprop"]}\"" : "";
			$rel      = ($img["rel"]      != null) ? "rel=\"{$img["rel"]}\"" : "";
			$class    = ($img["class"]    != null) ? "class=\"{$img["class"]}\"" : "";
			$target   = ($img["target"]   != null) ? "target=\"{$img["target"]}\"" : "";
			$text     = ($img["text"]     != null) ? $img["text"] : "";

			$svg = generate_svg($key, $img["alt"]);

			$html .= "<a {$itemprop} {$rel} {$class} {$target} {$link}>";
			$html .= "<span>";
			$html .= 	$svg;
			if ($img["itemprop"] == "telephone") {
				$html .= 	"<span {$itemprop}>{$text}</span>";
			} else {
				$html .= 	"{$text}";
			}
			$html .= "</span></a>";
		$html .= "</div>";
	}

	function generate_svg($title,$alt){
			//	Get the tiny SVG
			$svg_file = file_get_contents('svg/'.$title.'.svg');

			//	Add animation
			if($title != "calendar" && $title != "html5" && $title != "openbenches" )
			{
				$duration = rand (3,15);
				$animate = '><animate attributeName="rx" begin="0s" dur="'.$duration.'s" repeatCount="indefinite" values="0%;50%;50%;0%;0%;"/></rect>';
				$svg_file = preg_replace('/\/>/', $animate, $svg_file, 1);
			}
			//	Remove unecessary whitespace
			return preg_replace('/\s+/', ' ',$svg_file);
		}

$html .= '	</nav>
		</main>
	</div>
	<div class="speaker" id="bottomspeaker"></div>
</body>
</html>';

//	Minify
$html = str_replace(array("\r", "\n", "\t"), '', $html);
$html = str_replace(array("   "), ' ', $html);
$html = str_replace(array("> <"), "><", $html);
$html = str_replace(array("\" >"), "\">", $html);
$html = str_replace(array("  "), ' ', $html);
$html = str_replace(array("\" />"), "\"/>", $html);
$html = str_replace(array("   "), ' ', $html);
$html = str_replace(array("&nbsp;"), ' ', $html);

echo $html;
die();
